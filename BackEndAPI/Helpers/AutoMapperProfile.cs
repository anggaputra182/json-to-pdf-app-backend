namespace BackEndAPI.Helpers;

using AutoMapper;
using BackEndAPI.Entities;
//using BackEndAPI.Model;
using BackEndAPI.Models;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        //// CreateRequest -> Json
        CreateMap< Models.Json.CreateRequestJson, JsonDataModel>();
        //// UpdateRequest -> Json
        CreateMap<Models.Json.UpdateRequestJson, JsonDataModel>();

    }
}