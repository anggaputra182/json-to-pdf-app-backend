﻿namespace BackEndAPI.Entities;
using System;

public class JsonDataModel
{
    public int Id { get; set; }
    public string JSON { get; set; }
    //public class dataModel
    //{
    //    public int Id { get; set; }
    //    public ClientInformationModel Client_Information { get; set; }
    //    public FinancialsModel Financials { get; set; }
    //    public List<GoalsModel> Goals { get; set; }
    //    public List<InsurancesModel> Insurances { get; set; }
    //}

    //public class ClientInformationModel
    //{
    //    public string Client_Name { get; set; }
    //    public string Date_of_Birth { get; set;}
    //    public string Gender { get; set; }
    //    public string Marital_Status { get; set; }
    //    public string Employment_Status { get; set; }
    //}

    //public class FinancialsModel
    //{
    //    public string Monthly_Income { get; set; }
    //    public string Monthly_Expenses { get; set; }
    //    public string Savings { get; set; }
    //    public string Investments { get; set; }
    //    public string Debts { get; set; }
    //    public string Cashflow { get; set; }
    //    public string Net_Worth { get; set; }
    //}

    //public class GoalsModel
    //{
    //    public string Goal { get; set; }
    //    public string Target_Amount { get; set; }
    //    public string Monthly_Contribution { get; set; }
    //    public string Estimated_Duration { get; set; }
    //}

    //public class InsurancesModel
    //{
    //    public string Insurance_Type { get; set; }
    //    public string Coverage_Amount { get; set; }
    //    public string Monthly_Premium { get; set; }
    //    public string Provider { get; set; }
    //}
}

public class FileInfo
{
    public string FileName { get; set; }
    public byte[] FileContent { get; set; }
    public string ContentType { get; set; }
}

public class PdfSetting
{
    public string paperFormat { get; set; }
    public string marginTop { get; set; }
    public string marginBottom { get; set; }
    public string marginLeft { get; set; }
    public string marginRight { get; set; }
    public string fontType { get; set; }
    public string fontSize { get; set; }
    public string landscape { get; set; }
}