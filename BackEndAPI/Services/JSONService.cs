﻿namespace BackEndAPI.Services;

using AutoMapper;
using BackEndAPI.Entities;
using BackEndAPI.Models.Json;
using BackEndAPI.Repositories;


public interface IJsonService
{
    Task<IEnumerable<JsonDataModel>> GetAll();
    Task<JsonDataModel> GetById(int id);
    //Task Create(CreateRequestJson model, int id);
    Task<string> Create(CreateRequestJson model, int id);
    Task Update(int id, UpdateRequestJson model);
    Task Delete(int id);
    Task<JsonDataModel> GetDatalimit();
}
public class JSONService : IJsonService
{
    private IJsonRepository _JsonRepository;
    private readonly IMapper _mapper;

    public JSONService(
        IJsonRepository JsonRepository,
        IMapper mapper)
    {
        _JsonRepository = JsonRepository;
        _mapper = mapper;
    }

    //public Task Create(CreateRequestJson model)
    //{
    //    throw new NotImplementedException();
    //}

    //public Task Update(int id, UpdateRequestJson model)
    //{
    //    throw new NotImplementedException();
    //}

    //public Task Delete(int id)
    //{
    //    throw new NotImplementedException();
    //}

    public async Task<string> Create(CreateRequestJson model, int id)
    {
        var dataId = await _JsonRepository.GetById(id);

        if (dataId == null)
        {
            // map model to new user object
            var data = _mapper.Map<JsonDataModel>(model);

            // save user
            await _JsonRepository.Create(data);
            return ("JSON created");
        }
        else
        {
            // copy model props 
            _mapper.Map(model, dataId);
            //_mapper.Map(model, dataId);

            // save JSON
            await _JsonRepository.Update(dataId);
            return ("JSON updated");
        }
    }

    public async Task Delete(int id)
    {
        await _JsonRepository.Delete(id);
    }

    public async Task<IEnumerable<JsonDataModel>> GetAll()
    {
        return await _JsonRepository.GetAll();
    }

    public async Task<JsonDataModel> GetDatalimit()
    {
        var data = await _JsonRepository.GetDatalimit();

        if (data == null)
            throw new KeyNotFoundException("Json not found");

        return data;
    }

    public async Task<JsonDataModel> GetById(int id)
    {
        var data = await _JsonRepository.GetById(id);

        if (data == null)
            throw new KeyNotFoundException("Json not found");

        return data;
    }

    public async Task Update(int id, UpdateRequestJson model)
    {
        var data = await _JsonRepository.GetById(id);

        if (data == null)
            throw new KeyNotFoundException("Json not found");

        // copy model props to user
        //_mapper.Map(model, data);
        _mapper.Map(model, data);

        // save user
        await _JsonRepository.Update(data);
    }
}

