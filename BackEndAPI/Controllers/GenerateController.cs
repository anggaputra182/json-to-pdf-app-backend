﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using BackEndAPI.Models.Json;
using BackEndAPI.Services;

using DinkToPdf;
using DinkToPdf.Contracts;
using System;
using PuppeteerSharp.Media;
using PuppeteerSharp;
using Microsoft.AspNetCore.Rewrite;
using System.IO;
using System.Threading.Tasks;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using System.Text;
using Microsoft.Net.Http.Headers;
using BackEndAPI.Models.Json;

namespace BackEndAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenerateController : ControllerBase
    {
        private IJsonService _jsonService;
        private readonly IConverter _converter;
        public GenerateController(IJsonService jsonService, IConverter converter)
        {
            _jsonService = jsonService;
            _converter = converter;
        }


        static string FindChromeExecutable()
        {
            string[] possiblePaths = {
            Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
            Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
            //Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)
        };

            string[] chromePaths = possiblePaths
                .SelectMany(path => Directory.GetFiles(path, "chrome.exe", SearchOption.AllDirectories))
                .ToArray();

            return chromePaths.FirstOrDefault();
        }

        public enum PaperFormat
        {
            A0,
            A1,
            A2,
            A3,
            A4,
            A5,
            A6,
            Letter
        }

        private PuppeteerSharp.Media.PaperFormat MapToPuppeteerPaperFormat(string format)
        {
            switch (format)
            {
                case "A0":
                    return PuppeteerSharp.Media.PaperFormat.A0;
                case "A1":
                    return PuppeteerSharp.Media.PaperFormat.A1;
                case "A2":
                    return PuppeteerSharp.Media.PaperFormat.A2;
                case "A3":
                    return PuppeteerSharp.Media.PaperFormat.A3;
                case "A4":
                    return PuppeteerSharp.Media.PaperFormat.A4;
                case "A5":
                    return PuppeteerSharp.Media.PaperFormat.A5;
                case "A6":
                    return PuppeteerSharp.Media.PaperFormat.A6;
                case "Letter":
                    return PuppeteerSharp.Media.PaperFormat.Letter;
                default:
                    throw new ArgumentException("Invalid paper format specified.");
            }
        }

        [HttpGet]
        [Route("GeneratePdfV2")]
        public async Task<IActionResult> GeneratePdf(string paperFormat = "A4",
        string marginTop = "10mm",
        string marginBottom = "10mm",
        string marginLeft = "10mm",
        string marginRight = "10mm",
        string fontType = "Arial",
        int fontSize = 8,
        bool landscape = false)
        {
            try
            {
                string chromeExecutable = FindChromeExecutable();

                var launchOptions = new LaunchOptions { Headless = true, ExecutablePath = chromeExecutable };
                using var browser = await Puppeteer.LaunchAsync(launchOptions);
                using var page = await browser.NewPageAsync();

                string url = "http://localhost:3000/generate"; // Replace with your URL
                                                               //await page.GoToAsync(url, waitUntil: WaitUntilEvent.Load);
                await page.GoToAsync(url, new NavigationOptions() { WaitUntil = new[] { WaitUntilNavigation.Networkidle0 } });

                await Task.Delay(3000);
                int height = await page.EvaluateExpressionAsync<int>("document.body.scrollHeight");

                // Set font type and size using JavaScript
                await page.EvaluateFunctionAsync(@"(fontType, fontSize) => {
                    document.body.style.fontFamily = fontType;
                    document.body.style.fontSize = fontSize + 'px';
                }", fontType, fontSize);


                // Convert paperFormat string to enum
                var format = MapToPuppeteerPaperFormat(paperFormat);


                var stream = await page.PdfStreamAsync(new PdfOptions
                {
                    PrintBackground = true,
                    Height = height.ToString() + "px",
                    Format = format,
                    MarginOptions = new MarginOptions
                    {
                        Top = marginTop+"mm",
                        Bottom = marginBottom + "mm",
                        Left = marginLeft + "mm",
                        Right = marginRight + "mm"
                    },
                    DisplayHeaderFooter = true,
                    HeaderTemplate = "<div></div>",
                    FooterTemplate = "<div></div>",
                    Landscape = landscape,
                    PreferCSSPageSize = true
                });

                await browser.CloseAsync();

                var fileGen = new FileStreamResult(stream, "application/pdf");
                ContentDispositionHeaderValue contentDisposition = new ContentDispositionHeaderValue("inline");
                contentDisposition.SetHttpFileName("pdfData.pdf");
                Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet]
        [Route("GeneratePdfV1")]
        public async Task<IActionResult> GeneratePdfv1(string paperFormat = "A4",
        string marginTop = "10mm",
        string marginBottom = "10mm",
        string marginLeft = "10mm",
        string marginRight = "10mm",
        string fontType = "Arial",
        int fontSize = 8,
        bool landscape = false)
        {
            try
            {
                string chromeExecutable = FindChromeExecutable();

                var launchOptions = new LaunchOptions { Headless = true, ExecutablePath = chromeExecutable };
                using var browser = await Puppeteer.LaunchAsync(launchOptions);
                using var page = await browser.NewPageAsync();

                string url = "http://localhost:3000/generate"; // Replace with your URL
                                                               //await page.GoToAsync(url, waitUntil: WaitUntilEvent.Load);
                await page.GoToAsync(url, new NavigationOptions() { WaitUntil = new[] { WaitUntilNavigation.Networkidle0 } });

                await Task.Delay(3000);
                int height = await page.EvaluateExpressionAsync<int>("document.body.scrollHeight");

                // Set font type and size using JavaScript
                await page.EvaluateFunctionAsync(@"(fontType, fontSize) => {
                    document.body.style.fontFamily = fontType;
                    document.body.style.fontSize = fontSize + 'px';
                }", fontType, fontSize);


                // Convert paperFormat string to enum
                var format = MapToPuppeteerPaperFormat(paperFormat);


                var stream = await page.PdfStreamAsync(new PdfOptions
                {
                    PrintBackground = true,
                    Height = height.ToString() + "px",
                    Format = format,
                    MarginOptions = new MarginOptions
                    {
                        Top = marginTop + "mm",
                        Bottom = marginBottom + "mm",
                        Left = marginLeft + "mm",
                        Right = marginRight + "mm"
                    },
                    DisplayHeaderFooter = true,
                    HeaderTemplate = "<div></div>",
                    FooterTemplate = "<div></div>",
                    Landscape = landscape,
                    PreferCSSPageSize = true
                });

                await browser.CloseAsync();

                var fileGen = new FileStreamResult(stream, "application/pdf");
                ContentDispositionHeaderValue contentDisposition = new ContentDispositionHeaderValue("inline");
                contentDisposition.SetHttpFileName("pdfData.pdf");
                Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

                return File(stream, "application/pdf", "pdfData.pdf");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }


        [HttpGet]
        [Route("GetbyID/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var user = await _jsonService.GetById(id);
            return Ok(user);
        }

        [HttpGet]
        [Route("Getdatalimit")]
        public async Task<IActionResult> Getdatalimit()
        {
            var user = await _jsonService.GetDatalimit();
            return Ok(user);
        }

        [HttpPost]
        [Route("add/{id}")]
        public async Task<IActionResult> Create([FromForm] CreateRequestJson model, int id)
        {
            var status = await _jsonService.Create(model, id);
            return Ok(new { message = status });
        }

        //[HttpPost("{id}")]
        [HttpPost]
        [Route("UpdatebyID/{id}")]
        public async Task<IActionResult> Update(int id, UpdateRequestJson model)
        {
            await _jsonService.Update(id, model);
            return Ok(new { message = "JSON updated" });
        }

        [HttpPost("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _jsonService.Delete(id);
            return Ok(new { message = "JSON deleted" });
        }
    }
}
