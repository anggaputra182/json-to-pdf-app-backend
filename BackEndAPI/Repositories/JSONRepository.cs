﻿using Dapper;
using BackEndAPI.Entities;
using BackEndAPI.Helpers;
//using BackEndAPI.Model;

namespace BackEndAPI.Repositories;

public interface IJsonRepository
{
    Task<IEnumerable<JsonDataModel>> GetAll();
    Task<JsonDataModel> GetById(int id);
    //Task<StudentAdmissionDetailsModel> GetByName(string name);
    Task Create(JsonDataModel data);
    Task Update(JsonDataModel data);
    Task Delete(int id);
    Task<JsonDataModel> GetDatalimit();
}

public class JSONRepository : IJsonRepository
{
    private DataContext _context;

    public JSONRepository(DataContext context)
    {
        _context = context;
    }

    public async Task Create(JsonDataModel data)
    {
        using var connection = _context.CreateConnection();
        var sql = @"
            INSERT INTO JSONData (JSON)
            VALUES (@JSON)
        ";
        await connection.ExecuteAsync(sql, data);
    }

    public async Task Delete(int id)
    {
        using var connection = _context.CreateConnection();
        var sql = @"
            DELETE FROM JSONData 
            WHERE Id = @id
        ";
        await connection.ExecuteAsync(sql, new { id });
    }

    public async Task<IEnumerable<JsonDataModel>> GetAll()
    {
        using var connection = _context.CreateConnection();
        var sql = @"
            SELECT * FROM JSONData
        ";
        return await connection.QueryAsync<JsonDataModel>(sql);
    }

    public async Task<JsonDataModel> GetById(int id)
    {
        using var connection = _context.CreateConnection();
        var sql = @"
            SELECT * FROM JSONData 
            WHERE Id = @id
        ";
        return await connection.QuerySingleOrDefaultAsync<JsonDataModel>(sql, new { id });
    }

    public async Task<JsonDataModel> GetDatalimit()
    {
        using var connection = _context.CreateConnection();
        var sql = @"
            select * from JSONData limit 1
        ";
        return await connection.QuerySingleOrDefaultAsync<JsonDataModel>(sql);
    }

    public async Task Update(JsonDataModel data)
    {
        using var connection = _context.CreateConnection();
        var sql = @"
            UPDATE JSONData 
            SET JSON = @JSON
            WHERE Id = @Id
        ";
        await connection.ExecuteAsync(sql, data);
    }

    //public async Task<StudentAdmissionDetailsModel> GetByName(string name)
    //{
    //    using var connection = _context.CreateConnection();
    //    var sql = @"
    //        SELECT * FROM JSONData 
    //        WHERE StudentName = @name
    //    ";
    //    return await connection.QuerySingleOrDefaultAsync<StudentAdmissionDetailsModel>(sql, new { name });
    //}


}

