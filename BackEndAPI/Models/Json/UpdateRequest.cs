﻿namespace BackEndAPI.Models.Json;

using System.ComponentModel.DataAnnotations;
using BackEndAPI.Entities;

public class UpdateRequestJson
{
    //public int Id { get; set; }
    public ClientInformationUpdate Client_Information { get; set; }
    public FinancialsUpdate Financials { get; set; }
    public List<GoalsUpdate> Goals { get; set; }
    public List<InsurancesUpdate> Insurances { get; set; }
}

public class ClientInformationUpdate
{
    public string Client_Name { get; set; }
    public string Date_of_Birth { get; set; }
    public string Gender { get; set; }
    public string Marital_Status { get; set; }
    public string Employment_Status { get; set; }
}

public class FinancialsUpdate
{
    public string Monthly_Income { get; set; }
    public string Monthly_Expenses { get; set; }
    public string Savings { get; set; }
    public string Investments { get; set; }
    public string Debts { get; set; }
    public string Cashflow { get; set; }
    public string Net_Worth { get; set; }
}

public class GoalsUpdate
{
    public string Goal { get; set; }
    public string Target_Amount { get; set; }
    public string Monthly_Contribution { get; set; }
    public string Estimated_Duration { get; set; }
}

public class InsurancesUpdate
{
    public string Insurance_Type { get; set; }
    public string Coverage_Amount { get; set; }
    public string Monthly_Premium { get; set; }
    public string Provider { get; set; }
}