﻿namespace BackEndAPI.Models.Json;

using System.ComponentModel.DataAnnotations;
using BackEndAPI.Entities;

public class CreateRequestJson
{
    [Required]
    public string JSON { get; set; }
    //public int Id { get; set; }
    //public class addJson
    //{
    //    [Required]
    //    public ClientInformationModel Client_Information { get; set; }
    //    [Required]
    //    public FinancialsModel Financials { get; set; }
    //    [Required]
    //    public List<GoalsModel> Goals { get; set; }
    //    [Required]
    //    public List<InsurancesModel> Insurances { get; set; }
    //}

    //public class ClientInformationModel
    //{
    //    [Required]
    //    public string Client_Name { get; set; }
    //    [Required]
    //    public string Date_of_Birth { get; set; }
    //    [Required]
    //    public string Gender { get; set; }
    //    [Required]
    //    public string Marital_Status { get; set; }
    //    [Required]
    //    public string Employment_Status { get; set; }
    //}

    //public class FinancialsModel
    //{
    //    [Required]
    //    public string Monthly_Income { get; set; }
    //    [Required]
    //    public string Monthly_Expenses { get; set; }
    //    [Required]
    //    public string Savings { get; set; }
    //    [Required]
    //    public string Investments { get; set; }
    //    [Required]
    //    public string Debts { get; set; }
    //    [Required]
    //    public string Cashflow { get; set; }
    //    [Required]
    //    public string Net_Worth { get; set; }
    //}

    //public class GoalsModel
    //{
    //    [Required]
    //    public string Goal { get; set; }
    //    [Required]
    //    public string Target_Amount { get; set; }
    //    [Required]
    //    public string Monthly_Contribution { get; set; }
    //    [Required]
    //    public string Estimated_Duration { get; set; }
    //}

    //public class InsurancesModel
    //{
    //    [Required]
    //    public string Insurance_Type { get; set; }
    //    [Required]
    //    public string Coverage_Amount { get; set; }
    //    [Required]
    //    public string Monthly_Premium { get; set; }
    //    [Required]
    //    public string Provider { get; set; }
    //}
}

